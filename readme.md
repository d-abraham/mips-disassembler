This project is to write a partial disassembler for MIPS instructions.The input
 will be the 32-bit machine instructions that a compiler or assembler produces.
 The program then figures out what the original source instructions were that 
 created those 32-bit machine instructions and outputs them. The possible source
 instructions are: add, sub, and, or, slt, lw, sw, beq,bne.
