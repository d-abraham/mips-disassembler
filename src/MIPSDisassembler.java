/**
 *This project is to write a partial disassembler for MIPS instructions.The input
 * will be the 32-bit machine instructions that a compiler or assembler produces.
 * The program then figures out what the original source instructions were that
 * created those 32-bit machine instructions and outputs them. The possible source
 * instructions are: add, sub, and, or, slt, lw, sw, beq,bne.
 *
 * @author D-Abraham
 */
/*
        R-format =>
        6 bit  | 5 bit | 5 bit| 5bit | 5bit  | 6bit
        OPCODE | REG-1 | REG2 | REG3 |   X   | FUNC
        000000   00000   00000  00000  00000  000000

        I-format
        6 bit  | 5 bit | 5 bit| 16 bit
        OPCODE | REG-1 | REG2 | OFFSET
        000000   00000   00000  0000000000000000
*/

public class MIPSDisassembler {

    private static final int OpMask = 0xfc000000;
    private static final int rsMask = 0x03e00000;
    private static final int rtMask = 0x001f0000;
    private static final int rdMask = 0x0000f800;
    private static final int funcMask = 0x0000003f;
    private static final int offsetMask = 0x0000ffff;
    private static int address = 0x9A040;

    /**
     * Returns opcode(bit 26-31) of the instruction.
     * @param inst instruction to be disassembled
     * @return opcode
     */
    private static int getOP(int inst){
        return (inst & OpMask) >>> 26;
    }

    /**
     * Returns rs register(bit 21-25) of the instruction.
     * @param inst instruction to be disassembled
     * @return rs register
     */
    private static int getRS(int inst){
        return (inst & rsMask) >>> 21;
    }

    /**
     * Returns rt register(bit 16-20) of the instruction.
     * @param inst instruction to be disassembled
     * @return rt register
     */
    private static int getRT(int inst){
        return (inst & rtMask) >>> 16;
    }

    /**
     * Returns rd register(bit 11-15) of R-format instruction.
     * @param inst instruction to be disassembled
     * @return rd register
     */
    private static int getRD(int inst){
        return (inst & rdMask) >>> 11;
    }

    /**
     * Returns function(bit 0-5) of R-format instruction.
     * @param inst instruction to be disassembled
     * @return function
     */
    private static int getFunc(int inst){
        return inst & funcMask;
    }

    /**
     * Returns offset(bit 0-15) of I-format instruction.
     * @param inst instruction to be disassembled
     * @return rs register
     */
    private static int getOffset(int inst){
        return (short) (inst & offsetMask);
    }

    /**
     * Returns branch target address of I-format instruction:
     * 1- 16-bit short for offset
     * 2- The offset Signed shift << 2, then transfer 16-bit to 32-bit(branch) to be added to 32-bit PC
     * 3- Adding 32-bit branch to pc + 4
     * @param inst the instructions hex number
     * @return target address(pc)
     */
    private static String BranchAddress(int inst){
        short offset = (short) getOffset(inst);
        int branch = (offset << 2) + (address+4);
        //branch = branch + (address+4);
        return Integer.toHexString(branch);
    }

    /**
     * Returns assembly code of r-format instruction.
     * @param inst MIPS(hex) instruction
     * @return assembly code
     */
    private static String rFormat(int inst){
        String assembly= Integer.toHexString(address) + " - ";
        switch (getFunc(inst)){
            case 0x20:                     //add
                assembly += " add ";
                break;
            case 0x22:                     //sub
                assembly += " sub ";
                break;
            case 0x24:                     //and
                assembly += " and ";
                break;
            case 0x25:                     //or
                assembly += " or ";
                break;
            case 0x2a:                     //set less than
                assembly += " slt";
                break;
            default:
                assembly += " Unknown r-format";
                break;
        }
        assembly += "$" + getRD(inst) +", " + "$" + getRS(inst) + ", " + "$" + getRT(inst);
        address += 4;
        return assembly;
    }

    /**
     * Returns assembly code of i-format instruction.
     * @param inst MIPS(hex) instruction
     * @return assembly code
     */
    private static String iFormat(int inst){
        String assembly = Integer.toHexString(address) + " - ";
        switch (getOP(inst)) {
            case 0x23:                     //lw
                assembly += " lw $" + getRT(inst) + ", " + getOffset(inst) + "($" + getRS(inst) +
                        ")";
                address += 4;
                break;
            case 0x2b:                     //sw
                address += 4;
                assembly += " sw $" + getRT(inst) + ", " + getOffset(inst) + "($" + getRS(inst) +
                        ")";
                break;
            case 0x4:                      //beq
                assembly += " beq $" + getRS(inst) + ", $" + getRT(inst)+ " " + " address " +
                        BranchAddress(inst);
                address += 4;
                break;
            case 0x5:                      //bne
                assembly += " bne $" + getRS(inst) + ", $" + getRT(inst)+ " " + " address " +
                        BranchAddress(inst);
                address += 4;
                break;
            default:
                assembly += " Unknown r-format";
                break;
        }
        return assembly;
    }

    /**
     * The main static function, it will figure out the format of the instruction.
     * @param inst MIPS(hex) instruction
     * @return assembly code
     */
    public static String disassemble(int inst){
        if (getOP(inst) == 0){
            return rFormat(inst);
        }
        else if (getOP(inst) > 0){
            return iFormat(inst);
        }
        else {
            return "Error unknown command, code 1";
        }
    }

    //for testing>>
    public static void main(String[] args){

        int[] instructions = {0x032BA020, 0x8CE90014, 0x12A90003, 0x022DA822, 0xADB30020, 0x02697824, 0xAE8FFFF4,
                0x018C6020, 0x02A4A825, 0x158FFFF7, 0x8ECDFFF0};
        for (int inst : instructions) {
            System.out.println(disassemble(inst));
        }
    }
}
